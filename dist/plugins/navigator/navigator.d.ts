/// <reference path="../../tui2.d.ts" />
declare module tui.widget.ext {
    class Navigator extends Widget {
        private _activeItem;
        protected initRestriction(): void;
        protected initChildren(childNodes: Node[]): void;
        private checkScroll;
        protected init(): void;
        private collapse;
        private expand;
        private active;
        private drawItems;
        private _activeBy;
        activeBy(key: string, value: string): void;
        render(): void;
    }
}
